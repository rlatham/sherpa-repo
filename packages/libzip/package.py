from spack import *


class Libzip(CMakePackage):
    """ a C library for reading, creating, and modifying zip and zip64 archives"""

    homepage = "https://github.com/nih-at/libzip"
    url      = "https://github.com/nih-at/libzip/releases/download/v1.9.2/libzip-1.9.2.tar.gz"

    version('1.9.2', sha256='fd6a7f745de3d69cf5603edc9cb33d2890f0198e415255d0987a0cf10d824c6f')

    # depends_on('foo')
