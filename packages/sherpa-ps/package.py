from spack import *


class SherpaPs(AutotoolsPackage):
    """A SHERPA particle-level generator"""

    homepage = "https://gitlab.com/sherpa-team/sherpa/-/tree/229-hdf5-file-reader"
    url      = "https://gitlab.com/sherpa-team/sherpa/-/tree/229-hdf5-file-reader"
    git      = "https://gitlab.com/sherpa-team/sherpa.git"

    version('main', branch='229-hdf5-file-reader')

    depends_on('autoconf', type='build')
    depends_on('automake', type='build')
    depends_on('libtool',  type='build')
    depends_on('m4',       type='build')

    depends_on('lhapdf+mpi')
    depends_on('hdf5+mpi')
    depends_on('libzip')
    depends_on('fastjet')
    depends_on('sqlite@3.0:')

    def autoreconf(self, spec, prefix):
        autoreconf('--install', '--verbose', '--force')

    def configure_args(self):
        spec = self.spec
        args = []

        args.append("--enable-lhapdf={0}".format(spec["lhapdf"].prefix))
        args.append("--enable-hdf5include={0}".format(spec["hdf5"].prefix + "/include"))
        args.append("--enable-libhdf5={0}".format(spec["hdf5"].prefix + "/lib"))
        args.append("--enable-fastjet={0}".format(spec["fastjet"].prefix))
        args.append("--enable-mpi")
        args.append("--with-sqlite3={0}".format(spec["sqlite"].prefix))
        # the ME generator and the Particle-level generator both create a
        # "Sherpa" executable, so let's make it possible to distinguish between
        # the two.
        args.append("--program-suffix=-ps")

        args.append("CC={0}".format(spec["mpi"].mpicc))
        args.append("CXX={0}".format(spec["mpi"].mpicxx))

        return args
