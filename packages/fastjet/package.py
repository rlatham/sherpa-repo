from spack import *

class Fastjet(AutotoolsPackage):
    """A software package for jet finding in pp and e+e− collisions. It
    includes fast native implementations of many sequential recombination
    clustering algorithms, plugins for access to a range of cone jet finders
    and tools for advanced jet manipulation."""

    homepage = "https://fastjet.fr/"
    # super strange: spack gives me a 403 whenever I use spack to fetch this,
    # even though I can use command line tools like 'curl' and 'wget' just fine
    #url      = "https://fastjet.fr/repo/fastjet-3.4.0.tar.gz"
    url       = "https://web.cels.anl.gov/~robl/fastjet-3.4.0.tar.gz"


    version('3.4.0', 'ee07c8747c8ead86d88de4a9e4e8d1e9e7d7614973f5631ba8297f7a02478b91')
