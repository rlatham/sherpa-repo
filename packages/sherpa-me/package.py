from spack import *


class SherpaMe(AutotoolsPackage):
    """Sherpa is an event-generation framework for high-energy
particle collisions."""

    homepage = "https://sherpa-team.gitlab.io/"
    url      = "https://gitlab.com/shoeche/sherpa.git"
    git      = "https://gitlab.com/shoeche/sherpa.git"

    version('main', branch='me')

    depends_on('autoconf', type='build')
    depends_on('automake', type='build')
    depends_on('libtool',  type='build')
    depends_on('m4',       type='build')

    depends_on('lhapdf+mpi')
    depends_on('hdf5+mpi')
    depends_on('libzip')
    depends_on('mpi') # redunant, since we have 'hdf5+mpi' but make it explicit

    def autoreconf(self, spec, prefix):
        autoreconf('--install', '--verbose', '--force')

    def configure_args(self):
        spec = self.spec
        args = []

        args.append("--enable-lhapdf={0}".format(spec["lhapdf"].prefix))
        args.append("--enable-hdf5include={0}".format(spec["hdf5"].prefix + "/include"))
        args.append("--enable-libhdf5={0}".format(spec["hdf5"].prefix + "/lib"))
        args.append("--enable-mpi")
        args.append("--with-libzip={0}".format(spec["libzip"].prefix))
        # the ME generator and the Particle-level generator both create a
        # "Sherpa" executable, so let's make it possible to distinguish between
        # the two.
        args.append("--program-suffix=-me")
        args.append("CC={0}".format(spec["mpi"].mpicc))
        args.append("CXX={0}".format(spec["mpi"].mpicxx))

        return args
