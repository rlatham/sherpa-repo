from spack import *

class Lhapdf(AutotoolsPackage):
    """The LHAPDF parton density evaluation library"""

    homepage = "https://lhapdf.hepforge.org/"
    url      = "https://gitlab.com/hepcedar/lhapdf/"
    url      = "https://lhapdf.hepforge.org/downloads/?f=LHAPDF-6.5.3.tar.gz"

    version('6.5.3', '57435cd695e297065d53e69bd29090765c934936b6a975ff8c559766f2230359')

    variant('mpi', default=False, description="Enable MPI support for scalable I/O")

    depends_on("python")
    depends_on('mpi', when='+mpi')

    def configure_args(self):
        spec = self.spec
        args = []
        args.extend(['PYTHON_VERSION=3'])

        if '+mpi' in spec:
            args.extend([
                "--enable-mpi",
                "CC=%s"  % spec['mpi'].mpicc,
                "CXX=%s" % spec['mpi'].mpicxx
            ])
        return args
