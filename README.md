# SHERPA spack packages

This repository contains a collection of [spack](https://spack.io/)  packages to manage
building the SHERPA tools

For more about spack and what you can do with it, spack has lots of
[documentation](https://spack.readthedocs.io/en/latest/) and a good
[tutorial](https://spack.readthedocs.io/en/latest/tutorial_sc16.html).

## Installing

To add this repository to your spack installation, check out this proejct and add it:


   git clone https://gitlab.com/rlatham/sherpa-repo.git
   cd sherpa-repo
   spack repo add .
